package me.illuminatiproductions.tp;

import me.illuminatiproductions.tp.commands.Teleport;
import me.illuminatiproductions.tp.commands.TeleportAll;
import org.bukkit.plugin.java.JavaPlugin;

public final class TP extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        getCommand("tp").setExecutor(new Teleport());
        getCommand("tpall").setExecutor(new TeleportAll());
    }

}
