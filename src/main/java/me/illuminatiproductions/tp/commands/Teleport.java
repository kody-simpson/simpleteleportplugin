package me.illuminatiproductions.tp.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Teleport implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player){
            Player player = (Player) sender;

            if (args.length == 0){
                player.sendMessage(ChatColor.RED + "You need to enter some arguments.");
                player.sendMessage(ChatColor.YELLOW + "To teleport yourself: /tp <otherplayer>");
                player.sendMessage(ChatColor.YELLOW + "To teleport others: /tp <player> <otherplayer>");
            }else if(args.length == 1){
                Player target = Bukkit.getPlayer(args[0]); //Get player from command
                //Send you to that player
                try{
                    player.teleport(target.getLocation());
                }catch (NullPointerException e){
                    player.sendMessage(ChatColor.RED + "Player not found.");
                }
            }else if(args.length == 2){
                //First player
                Player playerToSend = Bukkit.getPlayer(args[0]);
                //Target player
                Player target = Bukkit.getPlayer(args[1]);
                //Teleport first player to second player
                try{
                    playerToSend.teleport(target.getLocation());
                }catch (NullPointerException e){
                    player.sendMessage(ChatColor.RED + "Player not found.");
                }
            }

        }


        return true;
    }
}
